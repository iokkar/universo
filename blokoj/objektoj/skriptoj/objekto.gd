extends "res://kerno/fenestroj/tipo_a1.gd"


var objekto # описываемый объект

# перезагружаем список описаний объектоа
func set_objekto(objekt):
	objekto = objekt
	$"VBox/body_texture/ItemList".clear()
	FillItemList()
	$VBox.set_visible(true)


# выводим описание объекта
func FillItemList():
#	if Global.logs:
#		print('objekto=',objekto)
	var _items = get_node("VBox/body_texture/ItemList")
	_items.add_item('имя объекта ' + objekto['nomo']['enhavo'])
	_items.add_item('uuid ' + objekto['uuid'])
	if objekto['integreco']:
		_items.add_item('целостность объекта ' + String(objekto['integreco']))
	if objekto['posedantoId']:
		_items.add_item('владелец объекта ID = ' + String(objekto['posedanto']['edges'].front()['node']['posedantoUzanto']['siriusoUzanto']['objId']))
		_items.add_item('ник владельца = ' + objekto['posedanto']['edges'].front()['node']['posedantoUzanto']['retnomo'])


