extends Spatial


const QueryObject = preload("queries.gd")


# warning-ignore:unused_signal
signal load_objektoj

# список id запросов, отправленных на сервер
var id_projekto_direkt_del = [] # список проектов на удаление
var subscription_id # id сообщения, по какому объекту, в каком проекте, какая задача изменена


func _ready():
	if Global.logs:
		print('запуск res://blokoj/kosmo/scenoj/space.tscn')
	$Control.camera = $camera

	# подключаем сигнал для обработки входящих данных
	var err = Net.connect("input_data", self, "_on_data")
	if err:
		print('error = ',err)

	Global.fenestro_kosmo = self
	# считываем размер экрана и задаём затемнение на весь экран
	$ui/loading.margin_right = get_node("/root").get_viewport().size.x
	$ui/loading.margin_bottom = get_node("/root").get_viewport().size.y
	# создаём свой корабль
	var ship = create_ship(Global.direktebla_objekto[Global.realeco-2])
	#если корабль игрока, то брать данные из direktebla_objekto

	$camera.translation=Vector3(
		Global.direktebla_objekto[Global.realeco-2]['koordinatoX'],
		Global.direktebla_objekto[Global.realeco-2]['koordinatoY'], 
		Global.direktebla_objekto[Global.realeco-2]['koordinatoZ']+22
	)
	add_child(ship,true)
	$camera.point_of_interest = ship
	$ui_armilo.sxipo = ship
	$"/root/Title/CanvasLayer/UI/konservejo/konservejo".load_konservejo()

	for i in get_children():
		if has_signal_custom(i,"new_way_point"):
			i.connect("new_way_point",self,"set_way_point")
	
	subscribtion_kubo()


func _on_data():
	var i_data_server = 0
	var q = QueryObject.new()
	for on_data in Net.data_server:
		var index = id_projekto_direkt_del.find(int(on_data['id']))
		if index > -1: # находится в списке удаляемых объектов
			# удаляем из списка
			var idx_prj = 0 #индекс массива для удаления
			for prj in Global.direktebla_objekto[Global.realeco-2]['projekto']['edges']:
				if prj['node']['uuid']==on_data['payload']['data']['redaktuUniversoProjekto']['universoProjekto']['uuid']:
					Global.direktebla_objekto[Global.realeco-2]['projekto']['edges'].remove(idx_prj)
				idx_prj += 1
			id_projekto_direkt_del.remove(index)
			Net.data_server.remove(i_data_server)
		elif int(on_data['id']) == subscription_id:
			if on_data['payload']['data']['universoObjektoEventoj']['evento']=='kubo_tasko':
#				if Global.logs:
#					print('===universoObjektoEventoj][evento]==kubo_tasko==',on_data['payload']['data'])
				# пришло сообщение, по какому объекту, в каком проекте, какая задача изменена
				sxangxi_tasko(on_data['payload']['data'])
			elif on_data['payload']['data']['universoObjektoEventoj']['evento']=='kubo_objekto':
				# изменение по объекту
#				print('=== редактируем объект по подписке ===',on_data['payload']['data']['universoObjektoEventoj']['objekto']['uuid'])
				sxangxi_objekto(on_data['payload']['data'])
			Net.data_server.remove(i_data_server)
		i_data_server += 1


# проверка, входит ли uuid в данный объект
#проверка по нисходящий (по всем детям-объектам)
func sercxado_uuid(uuid, objekt):
	var result = null
	if objekt.get('uuid'):
		if uuid == objekt.uuid:
			return objekt
	for ch in objekt.get_children():
		result = sercxado_uuid(uuid, ch)
		if result:
			return result
	return result


# поиск объекта, их частей и вложенных по uuid
func sercxo_objekto_uuid(uuid):
	for ch in get_children():
		if ch.is_in_group('create'):
			if (ch.uuid == uuid):
				return ch
	return null


# поиск объекта, их частей и вложенных по uuid
func sercxo_objektoj_uuid(uuid, uuid_mastro, uuid_posedi):
	for ch in get_children():
		if ch.is_in_group('create'):
			if (ch.uuid == uuid_mastro) or (ch.uuid == uuid_posedi) or\
					(ch.uuid == uuid):
				return ch
	return null


# меняем задачу объекту
func sxangxi_tasko(on_data):
	# пометка о нахождении нужного объекта
	var objekto = false
	# если эти изменения не по задаче
	if not on_data['universoObjektoEventoj']['tasko']:
		return
	var uuid = on_data['universoObjektoEventoj']['tasko']['posedanto']['edges'].front()['node']['posedantoObjekto']['uuid']
	for ch in get_children(): # проходим по всем созданным объектам (кроме управляемого корабля) в поисках нужного
		if sercxado_uuid(uuid,ch):
#			отправляю астероиду
#			if on_data['universoObjektoEventoj']['objekto']['uuid']==ch.uuid:
				# если это задача категории движения
			if on_data['universoObjektoEventoj']['projekto']['kategorio']['edges'].front()['node']['objId']==Net.kategorio_movado:
					# изменение маршрута движения
				if ch.is_in_group('create'):
					ch.sxangxi_itinero(on_data['universoObjektoEventoj']['projekto'],
						on_data['universoObjektoEventoj']['tasko'])
				# если проект стрельбы
			elif on_data['universoObjektoEventoj']['projekto']['kategorio']['edges'].front()['node']['objId']==Net.projekto_kategorio_pafado:
				ch.pafo(on_data['universoObjektoEventoj']['projekto'],
					on_data['universoObjektoEventoj']['tasko'])
				if on_data['universoObjektoEventoj']['tasko']['kategorio']['edges'].front()['node']['objId']==Net.tasko_kategorio_celilo:
					# сообщаем кораблю игрока о прицеливании в него
					pass
			objekto = true
	if !objekto: # если объект не найден, то нужно его добавить (если он не часть своего корабля)
		if sercxado_uuid(uuid,$ship):
			print('часть управляемого корабля uuid=',uuid)
		else:
			print('===найден новый объект!!! uuid=',uuid) #,' ===on_data=',on_data)


# изменяем характеристики объекта
func sxangxi_objekto(on_data):
#	if Global.logs:
#		print('===sxangxi_objekto==on_data=',on_data)
	var uuid = on_data['universoObjektoEventoj']['objekto']['uuid']
	var uuid_mastro
	var uuid_posedi
	if on_data['universoObjektoEventoj']['mastro']:
		uuid_mastro = on_data['universoObjektoEventoj']['mastro']['uuid']
	if on_data['universoObjektoEventoj']['posedi']:
		uuid_posedi = on_data['universoObjektoEventoj']['posedi']['uuid']
	var objekto = sercxo_objektoj_uuid(uuid, uuid_mastro, uuid_posedi)
	if objekto: # объект в космосе найден
		# если объект, которым управляем
		if Global.logs:
			print('===objekto==',uuid)
#		передавать и два внешних
		sxangxi_instance(objekto, on_data['universoObjektoEventoj'])
	else: # ранее объекта не было - добавляем его
		if Global.logs:
			print('ранее объекта не было - добавляем его = ' )
		analizo_item(on_data['universoObjektoEventoj']['objekto'])


# подписка на действие в кубе нахождения
func subscribtion_kubo():
	var q = QueryObject.new()
	subscription_id = Net.get_current_query_id()
#	Net.send_json(q.test_json(subscription_id))
	Net.send_json(q.kubo_json(subscription_id))


func has_signal_custom(node, sgnl) ->bool:
	if node == null:
		return false
	for i in node.get_signal_list():
		if i.name == sgnl:
			return true
	return false


const sxipo = preload("res://blokoj/kosmosxipoj/scenoj/sxipo_fremdulo.tscn")
const sxipo_modulo = preload("res://blokoj/kosmosxipoj/skriptoj/moduloj/sxipo.gd")
const base_ship = preload("res://blokoj/kosmosxipoj/scenoj/base_ship.tscn")
const debris = preload("res://blokoj/kosmosxipoj/scenoj/debris.tscn")
const espero = preload("res://blokoj/kosmostacioj/espero/espero_ekster.tscn")
const asteroido = preload("res://blokoj/asteroidoj/asteroido.tscn")
const ast_1 = preload("res://blokoj/asteroidoj/ast_1.tscn")
const ast_1_cr = preload("res://blokoj/asteroidoj/ast_1_cr.tscn")
const ast_2 = preload("res://blokoj/asteroidoj/ast_2.tscn")
const ast_2_cr = preload("res://blokoj/asteroidoj/ast_2_cr.tscn")
const ast_3 = preload("res://blokoj/asteroidoj/ast_3.tscn")
const ast_3_cr = preload("res://blokoj/asteroidoj/ast_3_cr.tscn")
const ast_4 = preload("res://blokoj/asteroidoj/ast_4.tscn")
const ast_4_cr = preload("res://blokoj/asteroidoj/ast_4_cr.tscn")
const ast_5 = preload("res://blokoj/asteroidoj/ast_5.tscn")
const ast_5_cr = preload("res://blokoj/asteroidoj/ast_5_cr.tscn")
const ast_shards_0 = preload("res://blokoj/asteroidoj/ast_shards_0.tscn")
const ast_shards_1 = preload("res://blokoj/asteroidoj/ast_shards_1.tscn")
const ast_shards_2 = preload("res://blokoj/asteroidoj/ast_shards_2.tscn")
const ast_ice_1 = preload("res://blokoj/asteroidoj/ast_ice_1.tscn")
const ast_ice_1_cr = preload("res://blokoj/asteroidoj/ast_ice_1_cr.tscn")
const ast_ice_2 = preload("res://blokoj/asteroidoj/ast_ice_2.tscn")
const ast_ice_2_cr = preload("res://blokoj/asteroidoj/ast_ice_2_cr.tscn")
const ast_ice_3 = preload("res://blokoj/asteroidoj/ast_ice_3.tscn")
const ast_ice_3_cr = preload("res://blokoj/asteroidoj/ast_ice_3_cr.tscn")
const ast_ice_4 = preload("res://blokoj/asteroidoj/ast_ice_4.tscn")
const ast_ice_4_cr = preload("res://blokoj/asteroidoj/ast_ice_4_cr.tscn")
const ast_ice_5 = preload("res://blokoj/asteroidoj/ast_ice_5.tscn")
const ast_ice_5_cr = preload("res://blokoj/asteroidoj/ast_ice_5_cr.tscn")
const ast_ice_shards_0 = preload("res://blokoj/asteroidoj/ast_ice_shards_0.tscn")
const ast_ice_shards_1 = preload("res://blokoj/asteroidoj/ast_ice_shards_1.tscn")
const ast_ice_shards_2 = preload("res://blokoj/asteroidoj/ast_ice_shards_2.tscn")

const AnalizoProjekto = preload("res://blokoj/kosmosxipoj/skriptoj/itineroj.gd")


# функция создания управляемого корабля
func create_ship(objecto):
	var ship = null
	
	if (objecto['resurso']['objId'] == 3)or(objecto['resurso']['objId'] == 12):# это корабль "Vostok U2" "Базовый космический корабль"
		ship = base_ship.instance()
		var sh = sxipo_modulo.new()
		if objecto['stato']['objId']==5:# полностью разрушенный корабль
			ship.add_child(debris.instance())
		else:
			sh.create_sxipo(ship, objecto)
	if not ship: # проверка, если такого корабля нет в программе
		return null
	ship.translation=Vector3(objecto['koordinatoX'],
		objecto['koordinatoY'], objecto['koordinatoZ'])
	if objecto['rotaciaX']:
		ship.rotation=Vector3(objecto['rotaciaX'],
			objecto['rotaciaY'], objecto['rotaciaZ'])
	ship.visible=true
	ship.uuid=objecto['uuid']
	ship.objekto = objecto.duplicate(true)
	ship.add_to_group('create')
	$camera.set_privot(ship)
	return ship


# добавляем объект в космос с настройкой местоположения
func add_objekto(objekto,item):
	objekto.translation=Vector3(item['koordinatoX'],
		item['koordinatoY'], item['koordinatoZ'])
	objekto.uuid = item['uuid']
	objekto.objekto = item.duplicate(true)
	objekto.rotation=Vector3(item['rotaciaX'],
		item['rotaciaY'], item['rotaciaZ'])
	add_child(objekto)
	objekto.add_to_group('create')


# создание астероида согласно его состояния
func krei_asteroido(asteroido,stato,tipo):
	if tipo == 21:
		if stato==1:
			asteroido.add_child(ast_1.instance())
		elif stato==2:
			asteroido.add_child(ast_1_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_shards_1.instance())
		else:
			asteroido.add_child(ast_shards_2.instance())
	elif tipo == 7:
		if stato==1:
			asteroido.add_child(ast_2.instance())
		elif stato==2:
			asteroido.add_child(ast_2_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_shards_1.instance())
		else:
			asteroido.add_child(ast_shards_2.instance())
	elif tipo == 22:
		if stato==1:
			asteroido.add_child(ast_3.instance())
		elif stato==2:
			asteroido.add_child(ast_3_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_shards_1.instance())
		else:
			asteroido.add_child(ast_shards_2.instance())
	elif tipo == 23:
		if stato==1:
			asteroido.add_child(ast_4.instance())
		elif stato==2:
			asteroido.add_child(ast_4_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_shards_1.instance())
		else:
			asteroido.add_child(ast_shards_2.instance())
	elif tipo == 24:
		if stato==1:
			asteroido.add_child(ast_5.instance())
		elif stato==2:
			asteroido.add_child(ast_5_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_shards_1.instance())
		else:
			asteroido.add_child(ast_shards_2.instance())
	elif tipo == 20:
		if stato==1:
			asteroido.add_child(ast_ice_1.instance())
		elif stato==2:
			asteroido.add_child(ast_ice_1_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_ice_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_ice_shards_1.instance())
		else:
			asteroido.add_child(ast_ice_shards_2.instance())
	elif tipo == 25:
		if stato==1:
			asteroido.add_child(ast_ice_2.instance())
		elif stato==2:
			asteroido.add_child(ast_ice_2_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_ice_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_ice_shards_1.instance())
		else:
			asteroido.add_child(ast_ice_shards_2.instance())
	elif tipo == 26:
		if stato==1:
			asteroido.add_child(ast_ice_3.instance())
		elif stato==2:
			asteroido.add_child(ast_ice_3_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_ice_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_ice_shards_1.instance())
		else:
			asteroido.add_child(ast_ice_shards_2.instance())
	elif tipo == 27:
		if stato==1:
			asteroido.add_child(ast_ice_4.instance())
		elif stato==2:
			asteroido.add_child(ast_ice_4_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_ice_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_ice_shards_1.instance())
		else:
			asteroido.add_child(ast_ice_shards_2.instance())
	elif tipo == 28:
		if stato==1:
			asteroido.add_child(ast_ice_5.instance())
		elif stato==2:
			asteroido.add_child(ast_ice_5_cr.instance())
		elif stato==3:
			asteroido.add_child(ast_ice_shards_0.instance())
		elif stato==4:
			asteroido.add_child(ast_ice_shards_1.instance())
		else:
			asteroido.add_child(ast_ice_shards_2.instance())


# функция анализа и добавления объекта в космос на основе пришедшей инфы с сервера
func analizo_item(item):
#	if Global.logs:
#		print('===analizo_item=',item)
	if item['resurso']['objId'] == 1:#объект станция Espero
		var state = espero.instance()
		add_objekto(state,item)
		state.add_to_group('state')
	elif (item['resurso']['tipo']['objId'] == 2):# тип - корабль
		var s = sxipo.instance()
		if item['stato'] and item['stato']['objId']==5:# полностью разрушенный корабль
			s.add_child(debris.instance())
		else:
			var sh = sxipo_modulo.new()
			sh.create_sxipo(s, item)
		add_objekto(s,item)
		# добавляем маршрут движения
		if item['projekto']:
			var projektoj = item['projekto']['edges']
			var analizo = AnalizoProjekto.new()
			analizo.analizo_projekto(projektoj,s)
		s.add_to_group('enemies')
	elif ((item['resurso']['objId'] == 7) or\
			((item['resurso']['objId'] >= 20) and\
			(item['resurso']['objId'] <= 28))) and\
			(item['koordinatoX']):# тип - астероид
#		подгружаем в зависимости от состояния модификации астероида грузим картинку
		var ast = asteroido.instance()
		krei_asteroido(ast,item['stato']['objId'],item['resurso']['objId'])
		add_objekto(ast,item)
		ast.add_to_group('asteroidoj')


#  поиск вложенного объекта
func sergxo_enmetajxo_objekto(objekto, item):
	for obj in objekto['ligilo']['edges']:
		if item['posedi']['uuid'] == obj['node']['ligilo']['uuid']:
			return obj['node']['ligilo']
		elif len(obj['node']['ligilo']['ligilo']['edges'])>0:
			var result = sergxo_enmetajxo_objekto(obj['node']['ligilo'], item)
			if result:
				return result
	return false


# изменяем вложенный объект
func sxangxi_enmetajxo_objekto(objekto, item):
	#груз в связи. на 16.08.20 - руда в астероиде или на складе корабля
	# ищем, есть ли такой объект в связи
	var sxangxi_obj
#	наличие в первом уровне, а ещё есть и второй уровень - внутри грузового модуля
	# объект в котором ресурс, передаётся во втором параметре - posedi
	# Находим объект, в котором ресурс
	if item['posedi']['uuid'] == item['mastro']['uuid']:
		# ресурс находится в гланом объекте
		sxangxi_obj = objekto['objekto']
	else:
		# ресурс где-то во вложенном объекте, ищём в каком
		sxangxi_obj = sergxo_enmetajxo_objekto(objekto['objekto'],item)
	var trovigxo = false # наличие
	for obj in sxangxi_obj['ligilo']['edges']:
		#ищем ресурс в переменной
		if obj['node']['ligilo']['uuid'] == item['objekto']['uuid']:
			# вносим изменения по данному объекту
			obj['node']['ligilo']['integreco'] = item['objekto']['integreco']
			obj['node']['ligilo']['volumenoInterna'] = item['objekto']['volumenoInterna']
			obj['node']['ligilo']['volumenoEkstera'] = item['objekto']['volumenoEkstera']
			obj['node']['ligilo']['volumenoStokado'] = item['objekto']['volumenoStokado']
			trovigxo = true
	# ищем модуль
	var modulo = null
	for ch in objekto.get_children():
		if ch.get('uuid'):
			if (ch.uuid == item['posedi']['uuid']):
				modulo = ch
				if trovigxo:
					# добавляем данныее
					ch.sxangxi_objekto(item)
				else:
					ch.krei_objekto(item)
				break
	if !trovigxo:
		# если нет такого объекта, то добавляем его
		sxangxi_obj['ligilo']['edges'].push_back({
			'node': {
				'uuid': item['objekto']['ligiloLigilo']['edges'].front()['node']['uuid'],
				'tipo': item['objekto']['ligiloLigilo']['edges'].front()['node']['tipo'].duplicate(true),
				'ligilo': {
					'uuid': item['objekto']['uuid'],
					'integreco': item['objekto']['integreco'],
					'nomo': {'enhavo': item['objekto']['nomo']['enhavo']},
					'volumenoInterna': item['objekto']['volumenoInterna'],
					'volumenoEkstera': item['objekto']['volumenoEkstera'],
					'volumenoStokado': item['objekto']['volumenoStokado'],
					'resurso': {'objId': item['objekto']['resurso']['objId']}
				}
			}
		})


# изменить данные по объекту, который есть в космосе
func sxangxi_instance(objekto, item):
	# если это внутри космического объекта
	if item['mastro']:
		sxangxi_enmetajxo_objekto(objekto, item)
		if objekto == $ship: # если это управляемый корабль
#			print('НАШ КОРАБЬ')
			Title.get_node("CanvasLayer/UI/konservejo/konservejo").FillItemList()
		return
	if not item['objekto']['kubo']: # объект исчез из космоса
		# удаляем из списка объектов в космосе
		var i = 0
		for obj in Global.objektoj:
			if item['objekto']['uuid'] == obj['uuid']:
				Global.objektoj.remove(i)
				break
			i += 1
		# удаляем из космоса
		objekto.queue_free()
		return
	# уточняем координаты
	var distance_to = objekto.translation.distance_to(Vector3(item['objekto']['koordinatoX'],
		item['objekto']['koordinatoY'], item['objekto']['koordinatoZ']))
#	if Global.logs:
#		print('===translation.distance_to=',distance_to)
#		print('===sxipo =',objekto.translation,'=uuid=',objekto.uuid)
#		print('===ship =',$ship.translation,'=uuid=',$ship.uuid)
#		print('===server =',Vector3(item['objekto']['koordinatoX'],
#			item['objekto']['koordinatoY'], item['objekto']['koordinatoZ']))
	if distance_to > 2000:# не помогает на дистанции разница увеличивается
		objekto.translation=Vector3(item['objekto']['koordinatoX'],
			item['objekto']['koordinatoY'], item['objekto']['koordinatoZ'])
		objekto.rotation=Vector3(item['objekto']['rotaciaX'],
			item['objekto']['rotaciaY'], item['objekto']['rotaciaZ'])
#	print('===item.inte=',item['objekto']['integreco'])
	objekto.integreco = item['objekto']['integreco']
	objekto.objekto['integreco'] = item['objekto']['integreco']
#	print('===nomo=',objekto.o)
#	if item['resurso']['objId'] == 1:#объект станция Espero
#		var state = espero.instance()
#		add_objekto(state,item)
#		state.add_to_group('state')
	if (item['objekto']['resurso']['tipo']['objId'] == 2)and(item['objekto']['koordinatoX']):# тип - корабль
#		проверяем, нужно ли менять визауализацию объекта
		if objekto == get_node('ship'): #это управляемый объект
			print('попали в данные управляемого объекта')
#			print('  ===item=',item)
			return
		else:
			if objekto.objekto['stato']['objId'] == item['objekto']['stato']['objId']:
#				print('оставляем визуализацию как есть')
				return
		if item['objekto']['stato']['objId']==5:# полностью разрушенный корабль
			# разрушаем все части корабля
			for ch in objekto.get_children():
				if ch is CollisionShape:
					ch.queue_free()
			# добавляем осколки
			objekto.add_child(debris.instance())
		objekto.objekto['stato']['objId'] = item['objekto']['stato']['objId']

	elif ((item['objekto']['resurso']['objId'] == 7) or\
			((item['objekto']['resurso']['objId'] >= 20) and\
			(item['objekto']['resurso']['objId'] <= 28))) and\
			(item['objekto']['koordinatoX']):# тип - астероид
#		проверить, нужно ли менять визауализацию объекта
		if objekto.objekto['stato']['objId'] == item['objekto']['stato']['objId']:
#			print('оставляем визуализацию как есть')
			return
#		else:
#			print('меняем визуализацию объекта')
		# сохраняем предыдущее состояние
		var old_ast 
		for ch in objekto.get_children():
			if ch is Spatial:
#				print('==name=',ch.name)
				old_ast = ch
				break
		old_ast.queue_free()
#		подгружаем в зависимости от состояния модификации астероида грузим картинку
		krei_asteroido(objekto,item['objekto']['stato']['objId'],item['objekto']['resurso']['objId'])
		objekto.objekto['stato']['objId'] = item['objekto']['stato']['objId']


func _on_space_load_objektoj():
	Title.get_node("CanvasLayer/UI/Objektoj/Window").distance_to($ship.translation)
	# и теперь по uuid нужно найти проект и задачу
	var projektoj = Global.direktebla_objekto[Global.realeco-2]['projekto']['edges']
	var analizo = AnalizoProjekto.new()
	analizo.analizo_projekto(projektoj,Global.fenestro_itinero)
	# если корабль полетел, то включаем таймер
	if len(Global.fenestro_itinero.itineroj)>0 and (not Global.fenestro_itinero.itinero_pause):
		$timer.start()
	# создаём остальные объекты в космосе
	for item in Global.objektoj:
		analizo_item(item)


func _on_Timer_timeout():
	var q = QueryObject.new()
	# Делаем запрос к бэкэнду
	Global.direktebla_objekto[Global.realeco-2]['koordinatoX'] = $ship.translation.x
	Global.direktebla_objekto[Global.realeco-2]['koordinatoY'] = $ship.translation.y
	Global.direktebla_objekto[Global.realeco-2]['koordinatoZ'] = $ship.translation.z
	Global.direktebla_objekto[Global.realeco-2]['rotationX'] = $ship.rotation.x
	Global.direktebla_objekto[Global.realeco-2]['rotationY'] = $ship.rotation.y
	Global.direktebla_objekto[Global.realeco-2]['rotationZ'] = $ship.rotation.z
#	var id = Net.get_current_query_id()
#	Net.net_id_clear.append(id)
#	print('x=',Global.direktebla_objekto[Global.realeco-2]['koordinatoX'],
#	'  y=',Global.direktebla_objekto[Global.realeco-2]['koordinatoY'],
#	'  z=',Global.direktebla_objekto[Global.realeco-2]['koordinatoZ'])
#	Net.send_json(q.objecto_mutation_ws($ship.uuid, $ship.translation.x, 
#			$ship.translation.y, $ship.translation.z,
#			$ship.rotation.x, 
#			$ship.rotation.y, $ship.rotation.z,
#			id
#		)
#	)


func _on_space_tree_exiting():
	#разрушаем все созданные объекты в этом мире
	for ch in get_children():
		if ch.is_in_group('create'):
			ch.free()
	Global.fenestro_kosmo = null




