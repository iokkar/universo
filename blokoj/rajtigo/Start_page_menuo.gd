extends Control


func _ready():
	pass # Replace with function body.

func _on_real_button_up():
	$CanvasLayer/UI/Popup.popup_centered()
	
func _on_com_button_up():
	$CanvasLayer/UI/Popup.popup_centered()


func _on_cap_button_up():
	$CanvasLayer/UI/Popup.popup_centered()


func _on_mezo_regions_button_up():
	$CanvasLayer/UI/Popup.popup_centered()

func _on_quick_actions_button_up():
	$CanvasLayer/UI/Popup.popup_centered()


func _on_notifications_button_up():
	$CanvasLayer/UI/Popup.popup_centered()


func _on_search_button_up():
	$CanvasLayer/UI/Popup.popup_centered()


func _on_user_button_up():
	$CanvasLayer/UI/Popup.popup_centered()


func _on_news_button_up():
	$CanvasLayer/UI/Popup.popup_centered()


func _on_communities_button_up():
	$CanvasLayer/UI/Popup.popup_centered()


func _on_academy_button_up():
	$CanvasLayer/UI/Popup.popup_centered()


func _on_encyclopedia_button_up():
	$CanvasLayer/UI/Popup.popup_centered()


func _on_komerco_button_up():
	$CanvasLayer/UI/Popup.popup_centered()


func _on_Profilo_button_up():
	$CanvasLayer/UI/Popup.popup_centered()


func _on_ad_button_up():
	$CanvasLayer/UI/Popup.popup_centered()


func _on_Taskoj_button_up():
	$CanvasLayer/UI/Popup.popup_centered()


func _on_RCentro_button_up():
	$CanvasLayer/UI/Popup.popup_centered()


func _on_Objektoj_button_up():
	$CanvasLayer/UI/Popup.popup_centered()


func _on_b_itinero_button_up():
	$CanvasLayer/UI/Popup.popup_centered()


func _on_interago_button_up():
	$CanvasLayer/UI/Popup.popup_centered()


func _on_eliro_button_up():
	$CanvasLayer/UI/eliro/eliro.popup_centered()


func _on_Button_button_up():
	$CanvasLayer/UI/Popup.popup_centered()
