extends Object
# Здесь будем хранить всё для запросов к бэкэнду по блоку "rajtigo"


# URL к API (авторизация)
const URL_AUTH = "https://t34.tehnokom.su/api/v1.1/registrado/"
# URL к API
const URL_DATA = "https://t34.tehnokom.su/api/v1.1/"


# Запрос авторизации
func auth_query(login, password):
	return JSON.print({ "query": "mutation { ensaluti(login: \"%s\", password: \"%s\") { status token message csrfToken uzanto { objId } } }" % [login, password] })


# Запрос никнейма
func get_nickname_query(id):
	return JSON.print({ "query": "query { universoUzanto(siriusoUzantoId: %s) { edges { node { uuid retnomo } } } }" % id })


